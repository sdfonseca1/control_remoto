<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Control de motores</title>

	<!-- ARCHIVOS BOOTSTRAP -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<script type="text/javascript" src="js/jquery-3.4.1.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/raphael.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/jquery.mapael.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/mexico.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/mapa.js" charset="utf-8"></script>
    <script type="text/javascript" src="js/funcion.js" charset="utf-8"></script>
    <!-- <style type="text/css">
        body {
            color: #5d5d5d;
            font-family: Helvetica, Arial, sans-serif;
        }

        h1 {
            font-size: 30px;
            margin: auto;
            margin-top: 50px;
        }

        .container {
            max-width: 725px;
            margin: auto;
        }
    </style> -->
</head>
<body>
    <img src="" alt="" width="100">
	<!-- Barra de navegación -->
    <?php
        require_once("modulos/barra_navegacion.php");
    ?>
	<!--------------------------------------- Enlace de las páginas --------------------------------------->
	<div class="container my-5 p-5">
        <?php
            $enlace = new Controlador();
            $enlace->enlacesPaginasControlador();
        ?>
    </div>
	<!-- ----------------------------------------------------------------------------------------------- -->	
	<!-- Pie de página -->

	<script type="text/javascript" src="js/jquery-3.4.1.min.js" ></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
	
</body>
</html>