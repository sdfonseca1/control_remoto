<?php
/* AUTOR:
*  FECHA DE CREACIÓN:
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN:
*  ANOTACIONES:
*/

/*	El controlador es el encargado de recibir las solicitudes de las vistas y en caso de que lo requiera, deberá validar
**	los datos que provengan de dichas vistas.
**	Parte de las buenas prácticas de programación, es muy recomendable agregar las siguientes líneas de código después
**	de que se declare una función:
**		UTILIDAD:
**		PRECONDICION: 
**		POSTCONDICIÓN:
**	UTILIDAD consiste en agregar una breve descripción de la acción que realiza tu función.
**	PRECONDICION deberá de llevar un texto narrativo de qué requerimientos necesita tu función para poder
**	trabajar.
**	POSTCONDICION será una breve descripción de cuál será el resultado una vez que la función se ejecute.
**	Adicionalmente se puede agregar debajo de POSTCONDICION, los modificadores de acceso, descripción de los parámetros
**	que son recebidos, entre otros. La anotación adecuada será la siguiente:
**		@private 	DESCRIPCION
**		@public 	DESCRIPCION
**		@protected	DESCRIPCION
**		@param		DESCRIPCION
**	No se obliga al usuario a utilizar lo anteriormente mencionado y si quiere cambiarlo, tiene toda la libertad de caambiarlo
**		
*/
/*REQUERIMIENTOS DE ARCHIVOS*/

	class Controlador{

		/*VARIABLES Y CONSTANTES*/
		
		public function pagina(){
			/*UTILIDAD: hace un llamado a la plantilla.
			  PRECONDICION: 
			  POSTCONDICIÓN: muestra la plantilla.
			*/
			include("vista/plantilla.php");
		}

		public function enlacesPaginasControlador(){
			/*UTILIDAD: genera la nueva página a la que se desea ir de la opción del menú.
			  PRECONDICION: 
			  POSTCONDICIÓN: la nueva página a la que se desea ir de la opción del menú es mostrada.
			*/
			if(isset($_GET['accion'])){	
				$enlaces = $_GET['accion'];
			}else{
				$enlaces = 'index=accion=inicio';
			}
			$respuesta = Paginas::enlacesPaginasModelo($enlaces);
			include($respuesta);	
		}

		//USUARIO
		public function ingresoUsuarioControlador(){
			/*UTILIDAD: realiza el ingreso del usuario al sistema.
			  PRECONDICION: los datos deben ser validados en el formulario.
			  POSTCONDICIÓN: redirige a la página de inicio del administrador o muestra error en el inicio de sesión.
			*/
			if (isset($_POST['clave']) && isset($_POST['contrasena'])) {
				$datos_controlador = array('clave' => $_POST['clave'],
										  'contrasena' => $_POST['contrasena']);
				$respuesta = Modelo::ingresoUsuarioModelo('usaurio', $datos_controlador);
				// var_dump($respuesta);
				if ($respuesta['usuario'] == $_POST['clave'] && $respuesta['contrasenha'] == $_POST['contrasena']){
					session_start();
					$_SESSION['usuario'] = $respuesta['usuario'];
					header('location:index.php?accion=coontrol');
				}else{
					header('location:index.php?accion=inicioSesionError_1');
				}
			}
		}

	}


?>