<?php
/* AUTOR:
*  FECHA DE CREACIÓN:
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN:
*  ANOTACIONES:
*/
/*	Encargado de recoger los valores en la barra de dirección para rederigir entre las distintas vistas.
**	Parte de las buenas prácticas de programación, es muy recomendable agregar las siguientes líneas de código después
**	de que se declare una función:
**		UTILIDAD:
**		PRECONDICION: 
**		POSTCONDICIÓN:
**	UTILIDAD consiste en agregar una breve descripción de la acción que realiza tu función.
**	PRECONDICION deberá de llevar un texto narrativo de qué requerimientos necesita tu función para poder
**	trabajar.
**	POSTCONDICION será una breve descripción de cuál será el resultado una vez que la función se ejecute.
**	Adicionalmente se puede agregar debajo de POSTCONDICION, los modificadores de acceso, descripción de los parámetros
**	que son recebidos, entre otros. La anotación adecuada será la siguiente:
**		@private 	DESCRIPCION
**		@public 	DESCRIPCION
**		@protected	DESCRIPCION
**		@param		DESCRIPCION
**	No se obliga al usuario a utilizar lo anteriormente mencionado y si quiere cambiarlo, tiene toda la libertad de caambiarlo
**		
*/
/*VARIABLES Y CONSTANTES*/

	class Paginas{

		public function enlacesPaginasModelo($enlacesModelo){
			/*UTILIDAD: lista blanca de valores permitidos en la dirección.
			  PRECONDICION: recibe el nuevo valor de la variale accion de la dirección.
			  POSTCONDICIÓN: muestra la página a la que se quería acceder.
			*/
			 /*Codigo de ejemplo*/
			if($enlacesModelo == "inicio"){
			   	$modulo = "vista/modulos/inicio.html";
			}
			else if ($enlacesModelo == "control") {
				$modulo = "vista/modulos/control.php";
			}
			else if ($enlacesModelo == "iniciarSesion") {
				$modulo = "vista/modulos/ingresoUsuario.php";
			}
			else if ($enlacesModelo == "NLE") {
				$modulo = "vista/modulos/nuevo_leon.php";
			}
			return $modulo;
			
		}
	}
?>	