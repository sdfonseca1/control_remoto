<?php
/* AUTOR:
*  FECHA DE CREACIÓN:
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN:
*  ANOTACIONES:
*/
/*	El modelo es el encargado de recibir las solicitudes del controlador y no deberá validar datos.
**	Parte de las buenas prácticas de programación, es muy recomendable agregar las siguientes líneas de código después
**	de que se declare una función:
**		UTILIDAD:
**		PRECONDICION: 
**		POSTCONDICIÓN:
**	UTILIDAD consiste en agregar una breve descripción de la acción que realiza tu función.
**	PRECONDICION deberá de llevar un texto narrativo de qué requerimientos necesita tu función para poder
**	trabajar.
**	POSTCONDICION será una breve descripción de cuál será el resultado una vez que la función se ejecute.
**	Adicionalmente se puede agregar debajo de POSTCONDICION, los modificadores de acceso, descripción de los parámetros
**	que son recebidos, entre otros. La anotación adecuada será la siguiente:
**		@private 	DESCRIPCION
**		@public 	DESCRIPCION
**		@protected	DESCRIPCION
**		@param		DESCRIPCION
**	No se obliga al usuario a utilizar lo anteriormente mencionado y si quiere cambiarlo, tiene toda la libertad de caambiarlo
**		
*/
/*REQUERIMIENTOS DE ARCHIVOS*/
	
	require_once("conexion.php");

	class Modelo extends Conexion{
		
		/*VARIABLES Y CONSTANTES*/

		//USUARIO
		public function ingresoUsuarioModelo($tabla, $datos_modelo){
			/*UTILIDAD: obtiene los datos del administrador buscado.
			  PRECONDICION: recibe el parámetro de clave y password del adminstrador.
			  POSTCONDICIÓN: regresa los datos del administrador si encontró los datos en la BD.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT usuario, contrasena FROM $tabla WHERE usuario=:clave AND contrasenha=:contra");
			$sentencia->bindParam(":clave", $datos_modelo["clave"], PDO::PARAM_STR);
			$sentencia->bindParam(":contrasenha", SHA($datos_modelo["contrasenha"]), PDO::PARAM_STR);
			$sentencia->execute();
			return $sentencia->fetch();
		}
	}

?>
