'use strict'

$(document).ready(function($) {

	// comet implementation
	var Comet = function (data_url) {
		this.timestamp = 0;
		this.url = data_url;  
		this.noerror = true;

		this.connect = function() {
			var self = this;

			$.ajax({
				type : 'get',
				url : this.url,
				dataType : 'json', 
				data : {'timestamp' : self.timestamp},
				success : function(response) {
					self.timestamp = response.timestamp;
					self.handleResponse(response);
					self.noerror = true;          
				},
				complete : function(response) {
					// send a new ajax request when this request is finished
					if (!self.noerror) {
						// if a connection problem occurs, try to reconnect each 1 second
						setTimeout(function(){ comet.connect(); }, 1000);           
					}else {
						// persistent connection
						self.connect(); 
					}
					self.noerror = false; 
				}
			});
		}

		this.disconnect = function() {}

		this.handleResponse = function(response) {
			if(response.msg == '\n'){
				// console.log(response.msg);
				// console.log('Archivo sin datos');
				// $('#content').append('<h4>Valor: '+response.msg+'</h4>');
			}else{
				$('#content').empty();
				var a = response.msg.split("\n");
				console.log(a);
				a.forEach(function(e){
					var elem = e.split(";");
					mostrarDatos(elem);
				});
			}
		}
	}

	var comet = new Comet('js/procesa.php');
	comet.connect();

	// $("label").click(function() {
	// 	alert("Click en botón");
	// });

});


function mostrarDatos(elemento){
	var estado;
	// var boton = $('#'+elemento[1]);
	elemento[0]==0?estado = "Apagado":estado="Encendido";
	$('#content').append("<div class='card col-4'>"+
  		"<div class='card-body'>"+
  			"<img src='imagenes/"+elemento[0]+".png' width='100'>"+
    		"<h4 class='card-title'>"+elemento[1]+"</h4>"+
			"<p class='card-text'>"+
			elemento[2]+": "+elemento[3]+
			"</p>"+
			"<div class='custom-control custom-switch'>"+
				"<input type='checkbox' class='custom-control-input' id='"+elemento[1]+"'>"+
				"<label class='custom-control-label card-text' for='"+elemento[1]+"'>"+estado+"</label>"+
			"</div>"+
		"</div>"+
	"</div>");
	estadoBoton(elemento[1], estado)
}

function estadoBoton(id, estado){
	if (estado == "Encendido") {
		$('#'+id).prop('checked', true);
	}else{
		$('#'+id).prop('checked', false);
	}
}