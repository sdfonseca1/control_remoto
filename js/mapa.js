$(document).ready(function($) {
	$(function () {
            $(".mapcontainer").mapael({
                map: {
                    // Set the name of the map to display
            		name: "mexico"
            	},
            	areas: {
                    "yucatan": {
                        // value: "yucatan",
                        href: "index.php?accion=control&estado=yucatan",
                        // tooltip: {content: "Yucatan"},
                        legend: "Yucatan"
                    },
                    "nuevo leon": {
                        value: "Nuevo Leon",
                        href: "index.php?accion=NLE",
                        tooltip: {content: "<span>Nuevo León</span><br/>"}
                    }
            	}
        });
    });
});