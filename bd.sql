CREATE TABLE usuario(
	usuario VARCHAR(20),
	contrasenha VARCHAR(40),
	PRIMARY KEY(usuario)
)ENGINE=InnoDB;

CREATE TABLE estados(){
	claveEstado VARCHAR(3) NOT NULL,
	nombreEstado VARCHAR(50) NOT NULL,
	PRIMARY KEY(claveEstado)
}

CREATE TABLE fabrica(
	claveFabrica INT(10) NOT NULL,
	lugar VARCHAR(3) NOT NULL,
	nombreFabrica VARCHAR(100),
	PRIMARY KEY(claveFabrica)
	FOREIGN KEY(lugar) REFERENCES estados(claveEstado)
)ENGINE=InnoDB;

CREATE TABLE motor(
	claveMotor INT(10) NOT NULL,
	descripcion VARCHAR(200) NOT NULL,
	estado BIT NOT NULL,
	temperatura INT NOT NULL,
	alerta VARCHAR NOT NULL,
	PRIMARY KEY(claveMotor)
)ENGINE=InnoDB;

CREATE TABLE fabrica_motor(
	claveFabrica INT(10) NOT NULL,
	claveMotor INT(10) NOT NULL,
	FOREIGN KEY(claveFabrica) REFERENCES fabrica(claveFabrica),
	FOREIGN KEY(claveMotor) REFERENCES motor(claveMotor)
)ENGINE=InnoDB;